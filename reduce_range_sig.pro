
FUNCTION reduce_range_sig, x, x0, x1

IF x LT 0 THEN BEGIN
  x = -x
  x = REDUCE_RANGE(x, x0, x1)
  x = -x
ENDIF ELSE BEGIN
  x = REDUCE_RANGE(x, x0, x1)
ENDELSE

RETURN, x
END
