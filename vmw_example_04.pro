;==============================================================================
; VARDYA-MIHALAS-WHITTAKER EQUATION OF STATE
;------------------------------------------------------------------------------
; EXAMPLE 04: Compare VMW EOS to the ltbl11.dat table used by the MURaM code
;==============================================================================

PRO vmw_example_04

name = 'example04'

; MURaM ltbl table gives the temperature and the pressure as functions of the
; density and specific internal energy per mass. All quantities are in CGS 
; units. All values are linear. 
ltbl = READ_MURAM_EOS(type = 'ltbl', file = 'ltbl11.dat')

; For a selected density (rho = 10^-8 = ltbl.rho[250]), we first extract 
; corresponding arrays of p and T.
p = REFORM(ltbl.p[250, *])
t = REFORM(ltbl.t[250, *])
tp = 2

;===============================================================================
; Compute the EOS
;===============================================================================

;-------------------------------------------------------------------------------
; Setup
;-------------------------------------------------------------------------------
error = 1.D-8 
initial_guess = '0.01pgas'
abundances = 'AG89'       
partition_function = 'irwin'

name = name + '_' + abundances

;-------------------------------------------------------------------------------
; Run the code
;-------------------------------------------------------------------------------
s = EOS_VMW(t, p, $
           initial_guess = initial_guess, $
           abundances = abundances, $
           partition_function = partition_function, $
           error = error, $
           /extended, $
           specific = 1, $
           output = 4, tp = tp)

; HELP, s
plot, t, s.etot                
oplot, ltbl.t[200, *], ltbl.eps

plot, t, s.rho                
oplot, ltbl.t[200, *], ltbl.rho

stop
END










