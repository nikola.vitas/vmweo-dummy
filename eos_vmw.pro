;+
; NAME:
;       EOS_VMW
;
; PURPOSE:
;
;       This function is IDL implementation of a simple equation of state
;       valid as long as Saha's ionization formula is valid. The approach
;       is based on the papers of Vardya (1961, 1965, 1966), an article
;       and the book of Mihalas (1967, 1974) and on a paper of Wittmann
;       (1974). The input variables are the gas pressure and the 
;       temperature. The ingredients are H, H+, H-, H2, H2+ and a selection
;       of non-H atoms (neutrals, single and double ionized). The code first
;       solves the system of the Saha equations of all atoms and the 
;       chemical equilibrium equations of the two molecules to get the
;       electron pressure. Optionally, some other quantities may be 
;       computed and outputed (density, partial pressures of all 
;       ingredients, number densities of all ingredients, internal or
;       specific internal energy, parameters of the computation). The 
;       full list of variables is:
;
;-------------------------------------------------------------------------------
;       Global variables:
;-------------------------------------------------------------------------------
;       nc                         Number of contributing atomic elements
;       nt                         Number of temperature grid points
;       np                         Number of pressure grid points
;
;-------------------------------------------------------------------------------
;       Input:
;-------------------------------------------------------------------------------
;       t          [nt, np]        Temperature
;       p          [nt, np]        Total gas pressure
;
;-------------------------------------------------------------------------------
;       Partial pressures:
;-------------------------------------------------------------------------------
;       phtot      [nt, np]        Total pressure of H particles (molecule = one part.)
;       pihtot     [nt, np]        Total fictious pressure of H nuclei
;       ph         [nt, np]        Partial pressure of neutral H atoms
;       phm        [nt, np]        Partial pressure of H- ions
;       php        [nt, np]        Partial pressure of H+ ions (free protons)
;       ph2        [nt, np]        Partial pressure of H2 molecules
;       ph2p       [nt, np]        Partial pressure of H2+ molecules
;       pel        [nt, np]        Electron pressure
;       pelh       [nt, np]        Pressure due to the hydrogen electrons
;       pelnonh    [nt, np]        Pressure due to the non-hydrogen electrons
;       pnonhtot   [nt, np]        Total pressure of all non-H atoms and ions 
;       pnonh      [nc-1, nt, np]  Total pressure of all non-H atoms and ions per specie
;       p1nonh     [nc-1, nt, np]  Pressure of non-H neutrals per specie
;       p2nonh     [nc-1, nt, np]  Pressure of non-H positive ions per specie 
;       p3nonh     [nc-1, nt, np]  Pressure of non-H twice positive ions per specie
;       pa         [nt, np]        Total pressure of all atoms and molecules (H and non-H)
;
;-------------------------------------------------------------------------------
;       Number density:
;-------------------------------------------------------------------------------
;       n          [nt, np]        Total density of free particles (e, atoms, ions, molecules)
;       nhtot      [nt, np]        Total density of H particles (molecule = one part.)
;       nhnuc      [nt, np]        Total density of H nuclei (corresponds to pihtot)
;       nh         [nt, np]        Density of neutral H atoms
;       nhm        [nt, np]        Density of H- ions
;       nhp        [nt, np]        Density of H+ ions (free protons)
;       nh2        [nt, np]        Density of H2 molecules
;       nh2p       [nt, np]        Density of H2+ molecules
;       nel        [nt, np]        Electron density
;       nelh       [nt, np]        Density of the hydrogen electrons
;       nelnonh    [nt, np]        Density of the non-H electrons
;       nnonhtot   [nt, np]        Density of all non-H atoms and ions 
;       nnonh      [nc-1, nt, np]  Density of all non-H atoms and ions per specie
;       n1nonh     [nc-1, nt, np]  Density of non-H neutrals per specie
;       n2nonh     [nc-1, nt, np]  Density of non-H positive ions per specie 
;       n3nonh     [nc-1, nt, np]  Density of non-H twice positive ions per specie
;       na         [nt, np]        Density of all atoms and molecules (H and non-H)
;
;-------------------------------------------------------------------------------
; Abundances:
;-------------------------------------------------------------------------------
;       abund      [nc]            Logarithmic abundances relative to the number of H
;                                    particles (abund[H] = 12)
;       relabund   [nc]            Linear abundaces relative to the total number of 
;                                   particles (relabund[H] \approx 0.9)
;       alpha      [nc]            Linear abundaces relative to the number of H
;                                    particles (alpha[H] = 1)
;
;-------------------------------------------------------------------------------
;       Other thermodynamical variables:
;-------------------------------------------------------------------------------
;       rho        [nt, np]        Density
;       mua        [nt, np]        Mean molecular weight for atoms and ions only
;       mue        [nt, np]        Mean molecular weight for electrons only
;       mu         [nt, np]        Total mean molecular weight
;
;-------------------------------------------------------------------------------
;       (Specific) Internal energy
;-------------------------------------------------------------------------------
;       etot       [nt, np]        Total energy
;       egas       [nt, np]        Translational energy
;       eh         [nt, np]        Contribution of H
;       ehm        [nt, np]        Contribution of H-
;       ehp        [nt, np]        Contribution of H+
;       eh2        [nt, np]        Contribution of H2
;       eh2p       [nt, np]        Contribution of H2+
;       ehep       [nt, np]        Contribution of He+
;       ehepp      [nt, np]        Contribution of He++
;       emp        [nt, np]        Contribution of metals+ 
;       empp       [nt, np]        Contribution of metals++
;
;-------------------------------------------------------------------------------
;       Iteration:
;-------------------------------------------------------------------------------
;       diffs      [nt, np]        Differences from the iteration routines
;       iters      [nt, np]        Number of iterations before the solution is reached
;       f          [7, nt, np]     Result of the mw_eos_pelfrompt function
;       pel0       [nt, np]        Initial guess for the electron pressure
;     
;
;-------------------------------------------------------------------------------
;       Relations between the partial pressures are following:
;-------------------------------------------------------------------------------
;       phtot = ph + phm + php + ph2 + ph2p
;       pihtot = ph + phm + php + 2*ph2 + 2*ph2p
;       pel = pelh + pelnonh
;       pelh = php + ph2p - phm
;       pelnonh = p2nonh + 2*p3nonh
;       pnonh = p1nonh + p2nonh + p3nonh
;       pnonhtot = sum pnonh
;       pa = phtot + pnonhtot
;       p = pel + pa
;
;
; AUTHOR:
;
;       Nikola Vitas
;       Instituto de Astrofisica de Canarias (IAC)
;       C/ Via Lactea, s/n
;       E38205 - La Laguna (Tenerife), Espana
;       Email: n.vitas@iac.es, nikola.vitas@gmail.com
;       Homepage: nikolavitas.blogspot.com
;
; CATEGORY:
;
;       Atoms.
;
; CALLING SEQUENCE:
;
;       WRITE_OPALEOS, data, filename = filename
;
; INPUTS:
;
;       data     = Structure. See READ_OPALEOS for the definition.
; 
;       filename = String, Name of the input file.
; 
; OUTPUTS:
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; EXAMPLE:
;
; DEPENDENCIES:
;       DSIGN, SIGN, REDUCE_RANGE, REDUCE_RANGE_SIG, LOAD_ATOMS, LOAD_ABUNDANCES, 
;       LOAD_ATOMIC_WEIGHTS, PF, PARTITION_IRWIN, POLY, SAHA, EOS_VMW_PEFROMP,
;       DIATOMICS, DIATOMICS_VARDYA (and other functions and data used by
;       libraries ATOMIC and DIATOMICS).
;
; MODIFICATION HISTORY:
;
;       Written by Nikola Vitas, June 2013.
;-
;================================================================================
; WRITE_OPALEOS, IDL routine by Nikola Vitas is licensed under a Creative Commons 
; Attribution-NonCommercial-ShareAlike 3.0 Unported License.
;
; This software is provided by NV ''as is'' and any express or implied warranties, 
; including, but not limited to, the implied warranties of merchantability and 
; fitness for a particular purpose are disclaimed. In no event shall NV be liable 
; for any direct, indirect, incidental, special, exemplary, or consequential 
; damages (including, but not limited to, procurement of substitute goods or 
; services; loss of use, data, or profits; loss of use, data, or profits; or 
; business interruption) however caused and on any theory of liability, whether 
; in contract, strict liability, or tort (including negligence or otherwise) 
; arising in any way out of the use of this software, even if advised of the 
; possibility of such damage.
;================================================================================

; todo for vmw
; + check the energy expression
; + include keyword for specific eint
; + replace molecb with sauval
; + replace reduce range stuff
; + add pfrompel routine
; + rename mw_eos_point to mw_eos_pelfromp
; + new Saha routine
; + add abundances to the output
; + add the keyword specifying the initial guess to the output
; ? make the EOS_VMW solution
; + interpolate it to (T, rho), (p, rho)
; + compare it again to OPAL


; - get kpi out to the main routine
; - evaluate mu
; - revive different initial conditions

; - add routines to write the EOS for mancha and for muram and
;   native/OPAL format and sav
; - make sure that the options for NaN's, logarithm and units are
;   on the top level of the wrapper



FUNCTION eos_vmw, temperature, pressure, tp = tp, $
                  abundances = abundances, $
                  initial_guess = initial_guess, $
                  contributors = contributors, $
                  partition_function = partition_function, $
                  specific = specific, $
                  eint_zero_level = eint_zero_level, $
                  extended = extended, $
                  error = error, $ 
                  output = output
      
;-------------------------------------------------------------------------------
; Set default values of keywords
;-------------------------------------------------------------------------------

IF NOT(KEYWORD_SET(partition_function)) THEN partition_function = 'Irwin'

IF NOT(KEYWORD_SET(output)) THEN output = 3

IF NOT(KEYWORD_SET(eint_zero_level)) THEN eint_zero_level = 'groundH'

IF NOT(KEYWORD_SET(initial_guess)) THEN initial_guess = '0.01pgas'


;-------------------------------------------------------------------------------
; Constants
;-------------------------------------------------------------------------------
kkerg   = 1.3806488D-16        ; erg K^-1                 (from NIST)
kkev    = 8.6173324D-5         ; eV K^-1                  (from NIST)
m_h     = 1.67333D-24          ; g per H atom
amu     = 1.660538921D-24      ; g                        (from NIST)
ev2erg  = 6.24150934D11        ; 1 erg = 6.241509D11 eV   (from NIST)
hh      = 6.62606957D-27       ; erg s                    (from NIST)
hm_ion1 = 0.754209             ; eV                       (Pekeris, Phys.Rev. 1958, 1962)

;-------------------------------------------------------------------------------
; For comparison with OPAL
;-------------------------------------------------------------------------------

;mwdir = './'
;opaldir = '../idlopal_v1.3/'

;m = READ_OPALEOS(mwdir + 'MWEOS_tpgrid___mancha_fine_A09__0.01pgas')
;o = READ_OPALEOS(opaldir + 'EOS5_data_X0.7374_Z0.0134_tpgrid')
;rhom = REFORM(m.data[*, *, 0])
;rhoo = REFORM(o.data[*, *, 0])
;rhom = MW_CLEAN_MATRIX(rhom)
;rhoo = MW_CLEAN_MATRIX(rhoo)
;rhom = REFORM(rhom[*, ip])
;rhoo = REFORM(rhoo[*, ip])

;em = REFORM(m.data[*, *, 1])
;eo = REFORM(o.data[*, *, 1])
;em = MW_CLEAN_MATRIX(em)
;eo = MW_CLEAN_MATRIX(eo)
;em = REFORM(em[*, ip])
;eo = REFORM(eo[*, ip])
;em = em/rhom


;===============================================================================
; Administration of variables
;===============================================================================

; input = set_output_size(temperature, pressure, tp = tp, /double)

; t = input.x
; p = input.y
; nx = input.nx
; ny = input.ny

t = DOUBLE(temperature)
p = DOUBLE(pressure)
sx = SIZE(t)
sy = SIZE(p)

; tp = 1 => T and P are vectors, the result is matrix
; tp = 2 => Check below

IF NOT KEYWORD_SET(tp) THEN tp = 1

IF tp EQ 2 THEN BEGIN
 IF TOTAL(sx-sy) NE 0 THEN BEGIN
   MESSAGE, 'T and p have to have same size. STOP.'
 ENDIF ELSE BEGIN
   IF sx[0] EQ 0 THEN BEGIN
     nx = 1
     ny = 1
   ENDIF
   IF sx[0] EQ 1 THEN BEGIN
     nx = sx[1]
     ny = 1
   ENDIF 
   IF sx[0] EQ 2 THEN BEGIN
     nx = sx[1]
     ny = sx[2]
   ENDIF
 ENDELSE
ENDIF ELSE BEGIN
 IF (sx[0] NE 1) OR (sy[0] NE 1) THEN BEGIN
   MESSAGE, 'When TP is set, both T and p have to be vectors (of any size).'
 ENDIF ELSE BEGIN
     nx = sx[1]
     ny = sy[1]
 ENDELSE
 t = t # (DBLARR(ny)+1)
 p = p ## (DBLARR(nx)+1)  
ENDELSE

t = REFORM(t, [nx, ny])
p = REFORM(p, [nx, ny])  

theta = ALOG10(EXP(1.0))/(kkev * t)

np = ny
nt = nx

;===============================================================================
; Load atomic data and select the ncon most abundant elements
;===============================================================================

atoms = LOAD_ATOMS(abund_source = abundances)
xyz = LOAD_ABUNDANCES(source = abundances, /xyz)

; If keyword CONTRIBUTORS is not set, then the first 82 elements of the periodic
; system are considered.
; CONTRIBUTORS - 1 is hydrogen, 2 is helium, etc. Hydrogen MUST be included.
; catoms[0] is always hydrogen
IF NOT(KEYWORD_SET(contributors)) THEN contributors = atoms[INDGEN(82)].z 
IF contributors[0] NE 1 THEN MESSAGE, 'Hydrogen MUST be included as a contributor.'
nc = N_ELEMENTS(contributors)
catoms = atoms[contributors-1]

;===============================================================================
; Define the additional variables
;===============================================================================

pel     = REFORM(DBLARR(nx, ny), [nx, ny])
pel0    = REFORM(DBLARR(nx, ny), [nx, ny])
f       = REFORM(DBLARR(6, nx, ny), [6, nx, ny])
iters   = REFORM(INTARR(nx, ny), [nx, ny])
diffs   = REFORM(DBLARR(nx, ny), [nx, ny])
pinit0  = REFORM(DBLARR(nx, ny), [nx, ny])

;===============================================================================
; Set the type of the initial guess for the iteration (default 'pureh')
; initial_guess = ['001pgas'|'pureh'|'hplusm'|'hplusmeanm'|'inheritance']
;===============================================================================

IF initial_guess EQ '0.01pgas' THEN pel0 = 0.01D0 * p

;; IF initial_guess EQ 'pureh' THEN $
;;   FOR ix = 0, nx-1 DO $
;;     FOR iy = 0, ny-1 DO $
;;       pel0[ix, iy] = EOS_PUREH(t[ix, iy], p[ix, iy], atoms = atoms, $
;;                         partition_function = partition_function)

;; IF initial_guess EQ 'hplusm' THEN $
;;   FOR ix = 0, nx-1 DO $
;;     FOR iy = 0, ny-1 DO $
;;       pel0[ix, iy] = EOS_HPLUSM(t[ix, iy], p[ix, iy], atoms = atoms, zmetal = 26, $
;;                         partition_function = partition_function)

;; IF initial_guess EQ 'hplusmeanm' OR initial_guess EQ 'inheritance' THEN $
;;   FOR ix = 0, nx-1 DO $
;;     FOR iy = 0, ny-1 DO $
;;       pel0[ix, iy] = EOS_HPLUSMEANM(t[ix, iy], p[ix, iy], atoms = atoms, zmetal = [12, 14, 26], $
;;                         partition_function = partition_function)

;; IF initial_guess EQ 'inheritance' THEN BEGIN
;;   PRINT, ': Warning! : Depending on how fine is T,p grid, this option may actually take '
;;   PRINT, '             more iteration steps than hplusmeanm or pureh'      
;; ENDIF



;===============================================================================
; Compute the partition function and Saha's term for every T and for every 
; contributor.
;===============================================================================
; # g2p and g3p are for hydrogen, ap and bp for all other contributors
; # This is done here and not in the iteration routine to save the computing
;   time.
; # Check the SAHAP routine to see what is computed there.
;-------------------------------------------------------------------------------

pf = PF(t, 1, data = partition_function)

;g2p = REFORM(EOS_VMW_SAHAP(theta, catoms[0].ion1, pf.u1, pf.u2), [nx, ny]) 
;g3p = REFORM(EOS_VMW_SAHAP(theta, hm_ion1, 1.0, pf.u1), [nx, ny])  

g2p = REFORM(SAHA(t, chi = catoms[0].ion1, u1 = pf.u1, u2 = pf.u2), [nx, ny])  
g3p = REFORM(SAHA(t, chi = hm_ion1, u1 = 1.0, u2 = pf.u1), [nx, ny])  


; We save all this for later computation of internal energy
ap = DBLARR(nx, ny, nc)
bp = DBLARR(nx, ny, nc)

FOR ic = 1, nc-1 DO BEGIN
  pf = PF(t, catoms[ic].z, data = partition_function)
  ap[*, *, ic] = SAHA(t, chi = catoms[ic].ion1, u1 = pf.u1, u2 = pf.u2)
  bp[*, *, ic] = SAHA(t, chi = catoms[ic].ion2, u1 = pf.u2, u2 = pf.u3)
ENDFOR

;===============================================================================
; Relative abundance      
;=============================================================================== 
; abund -> epsilon
; relabund -> nu
; alpha -> alpha
; ra_nonh -> nunonh
     
epsilon = catoms.abund
nu = 10.D^(epsilon)/TOTAL(10.D^(epsilon))
alpha = nu/nu[0]
; We also need to specify relative abundance between the non-H atoms as they
; are all together in the variables pn (and in nn).
nunonh = 10.D^(epsilon[1:nc-1])/TOTAL(10.D^(epsilon[1:nc-1]))


;===============================================================================
; Run the computation
;===============================================================================

FOR ix = 0, nx-1 DO BEGIN
  PRINT, ix
  FOR iy = 0, ny-1 DO BEGIN
    pel[ix, iy] = EOS_VMW_PEFROMP(t[ix, iy], p[ix, iy], $
                    pel0 = pel0[ix, iy], $
                    alpha = alpha, $
                    ap = REFORM(ap[ix, iy, *]), $
                    bp = REFORM(bp[ix, iy, *]), $
                    g2p = g2p[ix, iy], $
                    g3p = g3p[ix, iy], iter = iter, $
                    dif = dif, $
                    error = error, $
                    f = f0)
    iters[ix, iy] = iter
    diffs[ix, iy] = dif
    f[*, ix, iy] = f0   
  ENDFOR                                 
ENDFOR

;===============================================================================
; Compute additional diagnostics
;===============================================================================

IF output GT 0 THEN BEGIN

  pnonh  = REFORM(DBLARR(nc-1, nx, ny), [nc-1, nx, ny])
  p1nonh = REFORM(DBLARR(nc-1, nx, ny), [nc-1, nx, ny])
  p2nonh = REFORM(DBLARR(nc-1, nx, ny), [nc-1, nx, ny])
  p3nonh = REFORM(DBLARR(nc-1, nx, ny), [nc-1, nx, ny])

  rho     = REFORM(DBLARR(nx, ny), [nx, ny])
  eint    = REFORM(DBLARR(nx, ny), [nx, ny])
Hase,Demoulin, Sauval, Toon, Bernath, Goldman, Hannigan, Rinsland

  ; Specify and reform f-coefficients
  PRINT, '> Reform f values'
  f1 = REFORM(f[0, *, *], [nx, ny])
  f2 = REFORM(f[1, *, *], [nx, ny])
  f3 = REFORM(f[2, *, *], [nx, ny])
  f4 = REFORM(f[3, *, *], [nx, ny])
  f5 = REFORM(f[4, *, *], [nx, ny])
  fe = REFORM(f[5, *, *], [nx, ny])

  ; Hydrogen pressures:
  PRINT, '> Compute hydrogen pressures'
  pihtot     = REFORM(pel/fe, [nx, ny])     
  ph2        = REFORM(f5 * pihtot, [nx, ny]) 
  ph2p       = REFORM(f4 * pihtot, [nx, ny])
  phm        = REFORM(f3 * pihtot, [nx, ny])
  php        = REFORM(f2 * pihtot, [nx, ny])
  ph         = REFORM(f1 * pihtot, [nx, ny])
  phtot      = REFORM(ph + phm + php + ph2 + ph2p, [nx, ny])

  ; Test if both pihtot and phtot fullfill their definition.
;  plot, (phtot[*, 4]-ph[*,4]-php[*,4]-phm[*,4]-ph2[*,4]-ph2p[*,4])/phtot[*,4]
;  plot, (pihtot[*, 4]-ph[*,4]-php[*,4]-phm[*,4]-2*ph2[*,4]-2*ph2p[*,4])/pihtot[*,4]

  ; Non-hydrogen pressures:
  PRINT, '> Compute non-hydrogen pressures'
  pnonhtot   = REFORM(pihtot*(1.D0 - nu[0])/nu[0], [nx, ny])
  FOR ic = 0, nc-2 DO BEGIN
    PROGRESS, 'Done', ic, nc-1
    pnonh[ic, *, *]  = pnonhtot*nunonh[ic]
    p1nonh[ic, *, *] = pnonh[ic, *, *]/(1.D0+(ap[*, *, ic+1]/pel)*(1.D0 + (bp[*, *, ic+1]/pel)))  
    p2nonh[ic, *, *] = ap[*, *, ic+1]/pel*p1nonh[ic, *, *]
    p3nonh[ic, *, *] = bp[*, *, ic+1]/pel*p2nonh[ic, *, *]
  ENDFOR

  ; Electron pressures (pel is computed by the solver):
  pelh       = REFORM(php - phm + ph2p, [nx, ny])
  pelnonh    = REFORM(TOTAL(p2nonh, 1) + 2*TOTAL(p3nonh, 1), [nx, ny])

  ; Total pressure of atoms and molecules
  pa         = REFORM(phtot + pnonhtot, [nx, ny])

  ; Test if pa and pel sum up to the given p
;  plot, (p[*, 4]-pa[*,4]-pel[*,4])/p[*,4]

  ; mua, mue

  ; Mass density
  muprim     = TOTAL(alpha*catoms.w)/catoms[0].w
  rho        = m_h*(pel/fe)*muprim/(kkerg*t)

  ;----------------------------------------------------------------------
  ; Internal energy
  ;----------------------------------------------------------------------
  ; Internal energy is computed after Eq.71 (p.18 of Mihalas, 1967) with
  ; some adaptations. The zero-energy level of Mihalas is set at the 
  ; energy of protons. Here we set it at the energy of the ground state
  ; of H2 following Mihalas, Daeppen and Hummer (1988). We omit the
  ; radiation pressure. However, we include the contribution of H- ion
  ; and H2+ molecule.
  ;----------------------------------------------------------------------

  PRINT, '> Compute internal energy'

  ; (due to the number of formed molecules, proportional to the
  ; dissociation energy), numbers taken from Mihalas et al (1988)
  ; and converted to eV by eunits.pro.
  d0_h2  = 4.478D0/ev2erg
  d0_h2p = 2.6506257/ev2erg 

  ; Internal energy: translational component
  egas   = 1.5D0*p

  ; H (from the ground state of H2)
  eh     = ph/(kkerg*t) * (0.5*d0_h2)

  ; H- (from the ground state of H2)
  ehm    = phm/(kkerg*t) * (0.5*d0_h2 - hm_ion1/ev2erg)

  ; H+ (from the ground state of H2)
  ehp    = php/(kkerg*t) * (catoms[0].ion1/ev2erg + 0.5*d0_h2)

  ; H2 (vibrational and rotational), Vardya (1964, p.211,, Eq.37)
  eh2    = ph2 * (2.6757D0 - 1.4772D0*theta + 0.60602D0*theta^2 - 0.12427D0*theta^3 + 0.0097503D0*theta^4)

  ; H2+ (from the ground state of H2), Vardya (1964, p.211,, Eq.38)
  eh2p   = ph2p * (2.9216D0 - 2.0036D0*theta + 1.7231D0*theta^2 - 0.82685D0*theta^3 + 0.15253D0*theta^4)

  eh2p = eh2p + d0_h2 + catoms[0].ion1/ev2erg - d0_h2p 

  ; He+ and He++
  ehep   = ((p2nonh[0, *, *] + p3nonh[0, *, *])/(kkerg*t)*catoms[1].ion1)/ev2erg
  ehepp  = (p3nonh[0, *, *]/(kkerg*t)*catoms[1].ion2)/ev2erg

  ; Metals (Z > 2) + and ++
  emp     = REFORM(DBLARR(nx, ny), [nx, ny])
  FOR ic = 1, nc-2 DO $
    emp   = emp + (p2nonh[ic, *, *] + p3nonh[ic, *, *])/(kkerg*t) * catoms[ic+1].ion1/ev2erg
  empp    = REFORM(DBLARR(nx, ny), [nx, ny])
  FOR ic = 1, nc-2 DO $
    empp  = empp + p3nonh[ic, *, *]/(kkerg*t) * catoms[ic+1].ion2/ev2erg
  
  ; Reform energies
  egas   = REFORM(egas, [nx, ny])
  eh     = REFORM(eh, [nx, ny])
  ehm    = REFORM(ehm, [nx, ny])
  ehp    = REFORM(ehp, [nx, ny])
  eh2    = REFORM(eh2, [nx, ny])
  eh2p   = REFORM(eh2p, [nx, ny])
  ehep   = REFORM(ehep, [nx, ny])
  ehepp  = REFORM(ehepp, [nx, ny])
  emp    = REFORM(emp, [nx, ny])
  empp   = REFORM(empp, [nx, ny])
  
  ; If eint_zero_level = 'groundH' then the zero of the internal energy is as
  ; in the old MURaM tables. If not, it matches the OPAL tables.
  IF eint_zero_level EQ 'groundH' THEN BEGIN
    etot     = egas + (ehp + eh2 + ehm + eh2p) + ehep + ehepp + emp + empp
  ENDIF ELSE BEGIN
    etot     = egas + (ehp + eh2 + ehm + eh2p + eh) + ehep + ehepp + emp + empp
  ENDELSE

  etot     = REFORM(etot, [nx, ny])
  
  
  ; Energy to specific energy if keyword SPECIFIC is set.
  IF specific EQ 1 THEN BEGIN
    egas   = egas/rho
    ehp    = ehp/rho
    eh     = eh/rho
    ehm    = ehm/rho
    eh2    = eh2/rho
    eh2p   = eh2p/rho
    ehep   = ehep/rho
    ehepp  = ehepp/rho
    emp    = emp/rho
    empp   = empp/rho
    etot   = etot/rho
  ENDIF  
  
 
  
  ;============================================================
  ; Compute number densities
  ;============================================================

  IF output EQ 5 OR output EQ 3 THEN BEGIN

    ; Electron densites
    PRINT, '> Compute electron pressures'
    nelh       = pelh/(kkerg*t)
    nelnonh    = pelnonh/(kkerg*t)
    nel        = pel/(kkerg*t)  ; = nelh + nelnonh

    ; Total number of atoms and molecules
    na         = pa/(kkerg*t)

    ; Total number of all particles (p/kkerg*t):
    n          = na + nel  

    ; Hydrogen densities:
    PRINT, '> Compute hydrogen densities'
    nhnuc      = pihtot/(kkerg*t)
    nhtot      = phtot/(kkerg*t)
    nh2        = ph2/(kkerg*t)
    nh2p       = ph2p/(kkerg*t)
    nhm        = phm/(kkerg*t)
    nhp        = php/(kkerg*t)
    nh         = ph/(kkerg*t)
  
    ; Non-hydrogen densities:
    nnonhtot   = pnonhtot/(kkerg*t)
    nnonh      = pnonh
    n1nonh     = p1nonh
    n2nonh     = p2nonh
    n3nonh     = p3nonh

    PRINT, '> Compute non-hydrogen densities'  
    FOR ic = 0, nc-2 DO BEGIN
      PROGRESS, 'Done', ic, nc-1
      nnonh[ic, *, *]  = REFORM(pnonh[ic, *, *])/(kkerg*t)
      n1nonh[ic, *, *] = REFORM(p1nonh[ic, *, *])/(kkerg*t)
      n2nonh[ic, *, *] = REFORM(p2nonh[ic, *, *])/(kkerg*t)
      n3nonh[ic, *, *] = REFORM(p3nonh[ic, *, *])/(kkerg*t)
    ENDFOR

    n1nonh     = REFORM(n1nonh, [nc-1, nx, ny])
    n2nonh     = REFORM(n2nonh, [nc-1, nx, ny])
    n3nonh     = REFORM(n3nonh, [nc-1, nx, ny])

  ENDIF 

ENDIF 

PRINT, '> Save output files'

abundances = catoms.abund

; Output only electron pressure
IF output EQ 0 THEN BEGIN
  result = pel   
ENDIF

; Output only the basic variables (pressure, temperature, density and internal energy)
IF output EQ 1 THEN BEGIN
  result = {p:p, pel:pel, t:t, rho:rho, etot:etot, nc:nc, nt:nt, np:np, $
            contributors:contributors, abundances:abundances, xyz:xyz}
ENDIF

; Add the detailed pressures to the output
IF output EQ 2 THEN BEGIN
  result = {p:p, pel:pel, t:t, rho:rho, etot:etot, phtot:phtot, pihtot:pihtot, $
            ph:ph, phm:phm, php:php, ph2:ph2, ph2p:ph2p, pelh:pelh, pelnonh:pelnonh, $
            pnonhtot:pnonhtot, pnonh:pnonh, p1nonh:p1nonh, p2nonh:p2nonh, $
            p3nonh:p3nonh, pa:pa, nc:nc, nt:nt, np:np, $
            contributors:contributors, abundances:abundances, xyz:xyz}
ENDIF

; Add the detailed number densities instead of the detailed pressures
IF output EQ 3 THEN BEGIN
  result = {p:p, pel:pel, t:t, rho:rho, etot:etot, n:n, nhtot:nhtot, nhnuc:nhnuc, $
            nh:nh, nhm:nhm, nhp:nhp, nh2:nh2, nh2p:nh2p, nel:nel, nelh:nelh, $
            nelnonh:nelnonh, nnonhtot:nnonhtot, nnonh:nnonh, n1nonh:n1nonh, $
            n2nonh:n2nonh, n3nonh:n3nonh, na:na, nc:nc, nt:nt, np:np, $
            contributors:contributors, abundances:abundances, xyz:xyz, f:f}
ENDIF

; Add the detailed pressures and the detailed internal energy
IF output EQ 4 THEN BEGIN
  result = {p:p, pel:pel, t:t, rho:rho, etot:etot, phtot:phtot, $
            pihtot:pihtot, ph:ph, phm:phm, php:php, ph2:ph2, ph2p:ph2p, pelh:pelh, $
            pelnonh:pelnonh, pnonhtot:pnonhtot, pnonh:pnonh, p1nonh:p1nonh, $
            p2nonh:p2nonh, p3nonh:p3nonh, pa:pa, nc:nc, nt:nt, np:np, $
            egas:egas, ehep:ehep, ehepp:ehepp, ehp:ehp, eh:eh, emp:emp, $
            empp:empp, ehm:ehm, eh2p:eh2p, eh2:eh2, $
            contributors:contributors, abundances:abundances, xyz:xyz, f:f}
ENDIF


; Add the detailed number densities and the detailed internal energy
IF output EQ 5 THEN BEGIN
  result = {p:p, pel:pel, t:t, rho:rho, etot:etot, n:n, nhtot:nhtot, nhnuc:nhnuc, $
            nh:nh, nhm:nhm, nhp:nhp, nh2:nh2, nh2p:nh2p, nel:nel, nelh:nelh, $
            nelnonh:nelnonh, nnonhtot:nnonhtot, nnonh:nnonh, n1nonh:n1nonh, $
            n2nonh:n2nonh, n3nonh:n3nonh, na:na, nc:nc, nt:nt, np:np, $
            egas:egas, ehep:ehep, ehepp:ehepp, ehp:ehp, eh:eh, emp:emp, $
            empp:empp, ehm:ehm, eh2p:eh2p, eh2:eh2, $
            contributors:contributors, abundances:abundances, xyz:xyz, f:f}
ENDIF

; The basic variables plus the iteration parameters.
IF output EQ 6 THEN BEGIN
  result = {p:p, pel:pel, t:t, rho:rho, etot:etot, nc:nc, nt:nt, np:np, $
            diffs:diffs, iters:iters, error:error, f:f, pel0:pel0, $
            initial_guess:initial_guess}
ENDIF
           
RETURN, result
END
