; Procedure to render MURaM tables from VMW EOS. Already computed EOS is adjusted to match 
; the rtbl table. 
PRO compare_to_shelyag


; o(epsilon, p)  (rho -> 0, t -> 1)
o1 = READ_OPALEOS('/scratch/nv/EOS_PROJECT//idlopal_v1.3/EOS_tables/EOS5_data_X0.7374_Z0.0134_epsilonpgrid')

; o(T, p) (rho -> 0, epislon -> 1)
o2 = READ_OPALEOS('/scratch/nv/EOS_PROJECT//idlopal_v1.3/EOS_tables/EOS5_data_X0.7374_Z0.0134_tpgrid')

; s(T, p)
RESTORE, '/scratch/nv/EOS_PROJECT/MWEOS_v2.0/VMW_tpgrid_MANCHAlinearT_AG89_0.01pgas.sav'
sp = REFORM(s.p[0, *])
st = REFORM(s.t[*, 0])
; x(eps, p)
x = read_muram_eos(file = '/scratch/nv/idl/postmhd_v1.3/rtbl11.dat')

print, alog10(x.p[1])-alog10(x.p[0])
;    0.0256250
print, alog10(sp[1])-alog10(sp[0])  
;     0.020000000
; s.p is slightly finer.

; alog10(sp[296]) \approx 3
; alog10(x.p[34]) \approx 3

; Compare at alog10(p) = 3, epsilon(T)



!p.background = 16777215L
!p.color = 0L
WINDOW, xsi = 1400, ysi = 800
PLOT, st, s.etot[*, 296], /ylog, yr = [2.d11, 4.d13], /ys, thick = 2
OPLOT, o1.data[*, 296, 1], o1.xaxis, col =getcolor('orange'), thick = 2
OPLOT, o2.xaxis, o2.data[*, 296, 1], col =getcolor('red'), thick = 2
OPLOT, st, s.etot[*,296]-s.eh[*,296], thick = 2, psym = -1
OPLOT, x.t[*, 34], x.eps, col = getcolor('blue'), thick = 2

; Thus there is a good match between Shelyag and
; s.etot[*,296]-s.eh[*,296]

; error = 1.D-8 ; 1.D-12
; initial_guess = '0.01pgas'      ; ['0.01pgas'|'pureh'|'hplusm'|'hplusmeanm']
; abundances = 'AG89'              ; ['W71', 'AG89', 'G84', 'GS98', 'A05', 'A09', 'L09']
; partition_function = 'irwin'    ; ['irwin'|'sauval'|'gray'|'wittmann']
; 
; name = 'MANCHAlinearT'
; name = name + '_' + abundances
; 
; sizes = SIZE(s.etot)
; nx = sizes[1]
; nz = sizes[2]
; 
; record = {x:0.D, z:0.D, nx:0, ny:0, xaxis:FLTARR(nx), yaxis:FLTARR(nz), $
;           nv:0, xname:'', yname:'', varnames:STRARR(3), data:DBLARR(nx, nz, 3), $
;           mask:DBLARR(nx, nz)}    
; record.x = s.xyz.x
; record.z = s.xyz.z
; record.nx = nx
; record.ny = nz
; record.nv = 3
; record.xaxis = st
; record.yaxis = sp
; record.xname = 'T'
; record.yname = 'p'
; record.varnames = ['rho', 'eint', 'pel']
; record.mask = REFORM(DBLARR(nx, nz), [nx, nz])
; record.data[*, *, 0] = s.rho  ; density [g cm^-3]
; record.data[*, *, 1] = s.etot- s.eh ; ADJUSTED specific internal energy [erg / g]
; record.data[*, *, 2] = s.pel  ; electron pressure
; WRITE_EOS, record, filename = 'VMW_ShelyagStyle_tpgrid_'+name+'_'+initial_guess+'.eos'




END
