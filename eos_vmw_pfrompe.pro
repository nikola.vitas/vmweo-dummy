FUNCTION eos_vmw_pfrompe, t, pel, alpha = alpha, $
                          ap = ap, bp = bp, g2p = g2p, g3p = g3p, f = f
           
;-------------------------------------------------------------------------------
; Constants
;-------------------------------------------------------------------------------
kkerg   = 1.38066D-16          ; erg K^-1
kkev    = 8.6173324D-5         ; eV K^-1
m_h     = 1.67333D-24          ; g per H atom
ev2erg  = 6.241506D11
araderg = 7.5657D-15           ; erg cm^-3 T^-4

ncontr = N_ELEMENTS(alpha)
nunonh = alpha[1:ncontr-1]/TOTAL(alpha[1:ncontr-1])

;================================================================================
; Definition of the parameters
;================================================================================

; kpi is reverse value of the chemical equilibrium constant, 1/kp
kpi = DBLARR(2)
kpi[0] = DIATOMICS(t, 'H2+', data = 'sauvalandtatum1984', type = 'kp')
kpi[1] = DIATOMICS(t, 'H2', data = 'sauvalandtatum1984', type = 'kp')
kpi = 1./kpi
kpi = REDUCE_RANGE(kpi, 1D-30, 1.D30)


g4 = pel * kpi[0] 
g5 = pel * kpi[1] 
g2 = g2p/pelectron0   ; p(h+)/p(h) 
g3 = g3p/pelectron0   ; p(h)/p(h-) 
g3 = REDUCE_RANGE(g3, 1.0D-30, 1.0D30)
g3 = 1.D0/g3  ; p(h-)/p(h)
g1 = 0.D0

;-------------------------------------------------------------------------------
; Sum that runs over He and all metals.
; g1 = total number of electrons that are contributed by no-H atoms.
;-------------------------------------------------------------------------------
FOR k = 1, ncontr-1 DO BEGIN
  aa = ap[k]/pel
  bb = bp[k]/pel
  cc = 1.D0 + aa*(1.D0 + bb)
  g1 = g1 + alpha[k]/cc*aa*(1.D0 + 2.D0*bb)
ENDFOR
  
;-------------------------------------------------------------------------------
; Coefficients a, b, c, d, e
;-------------------------------------------------------------------------------

a = 1.D0 + g2 + g3
b = 2*(1.D0 + g2*g4/g5)

; I set a limit to b. It is reached when g2*g4>>g5. At low T
; g2 is small, g4>>g5. At high T, g2 is huge, but g4 and g5 are both
; tiny. 
b = b < 1.D10

c = g5
d = g2 - g3
e = g2*g4/g5

a = REDUCE_RANGE_SIG(a, 1.D-15, 1.D15)
d = REDUCE_RANGE_SIG(d, 1.D-15, 1.D15)

c1 = c*b^2 + a*d*b - e*a^2
c2 = 2.D0*a*e - d*b + a*b*g1
c3 = -(e + b*g1)

;-------------------------------------------------------------------------------
; This is the solution of the quadratic equation. 
;-------------------------------------------------------------------------------
f1 = 0.5D0*c2/c1
f1 = -f1 + DSIGN(1.D0, c1)*SQRT(f1^2 - c3/c1)
  
;-------------------------------------------------------------------------------
; Solve for f2, f3, f4, f5 and fe
;-------------------------------------------------------------------------------

; If the solution for f1 is below 1.D-14 (meaning that only one in
; 1.D14 H atoms is neutral), then it is certain that the number
; density of H-, H2 and H2+ is even much smaller. In that case the
; code was getting unstable. Instead, when this limit is reached, I
; switch to the full ionization of H and set the number of electrons
; released by H atoms to be identical to the number of protons.
IF f1 LT 1.D-14 THEN BEGIN
  f2 = 1.00
  f1 = 0.
  f3 = 0.
  f4 = 0.
  f5 = 0.  
ENDIF ELSE BEGIN
  f5 = (1.D0 - a*f1)/b
  f4 = e*f5
  f3 = g3*f1
  f2 = g2*f1
ENDELSE

; Compute the electron fraction
fe = f2 - f3 + f4 + g1

; In Mihalas' article, fe is equal to pe/ph (so, it can be
; large only in the case of the large ionization (large pe) and very
; small total H pressure (what is never the case in the solar
; atmosphere, though the pressure decreases with the increasing number
; of molecules. On the other hand, when there is no many free electrons
; this number becomes very small. To prevent numerical instabilities
; we thus limit its possible range of values.

fe = REDUCE_RANGE(fe, 1.D-30, 1.D30)

;-------------------------------------------------------------------------------
; Once we know all f's, we can find the total pressure of the H
; nuclei.
;-------------------------------------------------------------------------------
phtot = pel/fe

;-------------------------------------------------------------------------------
; If the fraction of H2 relative to the total H pressure is less than
; 1.D-4 then it means that the fraction of H2+ is even smaller. We can
; omit the electrons that are bound in H2+ in the charge conservation
; equation. 
;-------------------------------------------------------------------------------

; const7 is the updated fraction of the electrons when the
; contribution from H2+ is omitted (f4).
IF(f5 LE 1.0D-4) THEN BEGIN
  const6 = g5/pel * f1^2
  const7 = f2 - f3 + g1

  FOR k = 0, 4 DO BEGIN
    f5 = phtot*const6
    f4 = e*f5
    fe = const7 + f4
    phtot = pel/fe
  ENDFOR
ENDIF 
  
p = pel * (1.D0 + (f1 + f2 + f3 + f4 + f5 + TOTAL(alpha[1:ncontr-1]))/fe )

f = [f1, f2, f3, f4, f5, fe]

RETURN, p
END
