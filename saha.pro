
; The result is dimensionless if neither pel nor nel are set.
; If one of them is set, then the dimension of the output is equal to
; the dimension of that variable. 

; z = 1 -> hydrogen
; r = 0 -> neutral


FUNCTION saha, t, chi=chi, z = z, r = r, u1 = u1, u2 = u2, pel = pel, nel = nel


kkerg   = 1.3806488D-16        ; erg K^-1                 (from NIST)
kkev    = 8.6173324D-5         ; eV K^-1                  (from NIST)
mel     = 9.10939D-28          ; g                        (from NIST)
hh      = 6.62606957D-27       ; erg s                    (from NIST)
hm_ion1 = 0.754209             ; eV                       (Pekeris, Phys.Rev. 1958, 1962)

IF NOT KEYWORD_SET(u1) OR NOT KEYWORD_SET(u2) THEN BEGIN
  IF NOT KEYWORD_SET(z) THEN BEGIN
    MESSAGE, 'Neither the partition functions are set, nor the atomic number Z.'
  ENDIF ELSE BEGIN
    IF TOTAL(SIZE(r)) EQ 0 THEN BEGIN
      MESSAGE, 'r must be set when u1 and u2 are not'
    ENDIF 
    IF r NE 0 AND r NE 1 THEN BEGIN
      MESSAGE, 'r can be only 0 or 1'
    ENDIF
    u = PF(t, z, data = 'gray')
    log = EXECUTE('u1 = u.u'+STRCOMPRESS(STRING(r+1), /rem))
    log = EXECUTE('u2 = u.u'+STRCOMPRESS(STRING(r+2), /rem))
  ENDELSE
ENDIF

IF NOT KEYWORD_SET(chi) THEN BEGIN
  IF NOT KEYWORD_SET(z) THEN BEGIN
    MESSAGE, 'When chi is not set, it is mandatory to set Z.'
  ENDIF ELSE BEGIN
    chi = LOAD_IONIZATION_ENERGIES(z, r)
  ENDELSE
ENDIF

lambda32 = 2*(2*!dpi*mel*kkerg*t/hh^2)^1.5

sahaexp = EXP(-chi/(kkev*t))

saha = (u2/u1)*lambda32*sahaexp*(kkerg*T)

IF KEYWORD_SET(pel) THEN saha = saha/pel

IF KEYWORD_SET(nel) THEN saha = saha/nel

RETURN, saha
END
