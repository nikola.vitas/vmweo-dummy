FUNCTION read_muram_eos, file = file, type = type

IF NOT KEYWORD_SET(type) THEN BEGIN
  type = STRSPLIT(file, '/', /extract)
  nwords = N_ELEMENTS(type)
  type = STRMID(type[nwords-1], 0, 4)
ENDIF

IF type NE 'ltbl' AND type NE 'rtbl' AND type NE 'stbl' THEN $
  MESSAGE, 'read_muram_eos: STOP. Unknown type of table.'

PRINT, 'read_muram_eos: Reading the '+type+' table: '
PRINT, '  ' + file 

IF type EQ 'ltbl' THEN BEGIN 

  neps = 0
  nrho = 0
  OPENR, un1, file, /get_lun
  READF, un1, neps, nrho
  rhoeos = FLTARR(nrho)
  epseos = FLTARR(neps)
  READF, un1, epseos
  READF, un1, rhoeos
  peos = FLTARR(nrho, neps)
  teos = FLTARR(nrho, neps)
  aa = 0.
  bb = 0.
  FOR ie = 0, neps-1 DO BEGIN
    FOR ir = 0, nrho-1 DO BEGIN
      READF, un1, aa, bb
      peos[ir, ie] = aa
      teos[ir, ie] = bb
    ENDFOR
  ENDFOR
  FREE_LUN, un1

  PRINT, '  t, log(p) = f(rho, eps)'
  PRINT, '  nrho = ', nrho
  PRINT, '  neps = ', neps

  eos = {neps:neps, nrho:nrho, rho:rhoeos, eps:epseos, t:teos, p:peos}
ENDIF

IF type EQ 'rtbl' THEN BEGIN

  np = 0
  neps = 0  
  OPENR, un1, file, /get_lun
  READF, un1, np, neps
  peos = FLTARR(np)
  epseos = FLTARR(neps)
  READF, un1, peos
  READF, un1, epseos
  rhoeos = FLTARR(neps, np)
  teos = FLTARR(neps, np)
  aa = 0.
  bb = 0.
  FOR ip = 0, np-1 DO BEGIN
    FOR ie = 0, neps-1 DO BEGIN
      READF, un1, aa, bb
      rhoeos[ie, ip] = aa
      teos[ie, ip] = bb
    ENDFOR
  ENDFOR

  FREE_LUN, un1

  PRINT, '  t, rho = f(eps, p)'
  PRINT, '  neps = ', neps
  PRINT, '  np = ', np

  eos = {neps:neps, np:np, rho:rhoeos, eps:epseos, t:teos, p:peos}
ENDIF

nr1 = 1000
rmin = 1.D-9
rmax = 1.D-5
rho0 = DBLARR(nr1)
FOR ir = 0, nr1-1 DO $
  rho0[ir] = 10.D0^(ALOG10(rmin) + ir*(ALOG10(rmax)-ALOG10(rmin))/(nr1-1))

; MURaM ltbl rho-axis:
;nr1 = 1000
;rmin = 1.D-9
;delrho = 4.D-3
;rho1 = DINDGEN(nr1)*delrho + ALOG10(rmin)
;rho1 = 10.^rho1

; MURaM rtbl p-axis
; p0 = DINDGEN(200)*0.025625D0 + 2.125D0 

; MURaM rtbl eint-axis
; eint0 = DINDGEN(200)*3.3728D10 + 2.D11

RETURN, eos
END

