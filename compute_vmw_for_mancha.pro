;+
; NAME:
;       COMPUTE_VMW_FOR_MANCHA
;
; PURPOSE:
;
;       This function serves as a caller to EOS_VMW that solves the Vardya-
;       -Mihalas-Whittaker Equation of State (EOS) for specified temperature
;       and pressure. The results are saved in a .eos file (containg the 
;       density, specific internal energy per mass and electron pressure) and
;       a .sav file (containing in addition all partial densities and internal 
;       energies). All units are in CGS. Note that the internal energy is 
;       set by default so that the energy of dissociation of the H2 molecule
;       is omitted (the zero energy for hydrogen is at the ground level of
;       neutral H), although the H2 formation is included in the equations. 
;       This convention is more suitable for the realistic simulations with 
;       MANCHA as it provides better stability at cool temperatures in the 
;       solar atmosphere. The EOS tables rendered in this way correspond to 
;       the tables used in early MURaM versions (see theses of Voegler and
;       Shelyag). If the H2 energy is taken into account, the generated tables
;       match the OPAL tables.
;      
; AUTHOR:
;
;       Nikola Vitas
;       Instituto de Astrofisica de Canarias (IAC)
;       C/ Via Lactea, s/n
;       E38205 - La Laguna (Tenerife), Espana
;       Email: n.vitas@iac.es, nikola.vitas@gmail.com
;       Homepage: nikolavitas.blogspot.com
;
; CATEGORY:
;
;       EOS.
;
; CALLING SEQUENCE:
;
;       COMPUTE_VMW_FOR_MANCHA, taxis = taxis, paxis = paxis, file_tpgrid = file_tpgrid
;
; INPUTS:
; 
;       taxis       = temperature axis
;       paxis       = pressure axis
;       file_tpgrid = name of output file (WITHOUT extension!) 
; 
; OUTPUTS:
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; EXAMPLE:
;
; DEPENDENCIES:
;
; MODIFICATION HISTORY:
;
;       Written by Nikola Vitas, June 2013.
;-
;================================================================================
; COMPUTE_VMW_FOR_MANCHA, IDL routine by Nikola Vitas is licensed under a 
; Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
;
; This software is provided by NV ''as is'' and any express or implied warranties, 
; including, but not limited to, the implied warranties of merchantability and 
; fitness for a particular purpose are disclaimed. In no event shall NV be liable 
; for any direct, indirect, incidental, special, exemplary, or consequential 
; damages (including, but not limited to, procurement of substitute goods or 
; services; loss of use, data, or profits; loss of use, data, or profits; or 
; business interruption) however caused and on any theory of liability, whether 
; in contract, strict liability, or tort (including negligence or otherwise) 
; arising in any way out of the use of this software, even if advised of the 
; possibility of such damage.
;================================================================================

PRO compute_vmw_for_mancha, taxis = taxis, paxis = paxis, file_tpgrid = file_tpgrid, $
                            abundances = abundances

;-------------------------------------------------------------------------------
; Setup -> Default for MANCHA
;-------------------------------------------------------------------------------
error = 1.D-8 ; 1.D-12
initial_guess = '0.01pgas'      ; ['0.01pgas'|'pureh'|'hplusm'|'hplusmeanm']
partition_function = 'irwin'    ; ['irwin'|'sauval'|'gray'|'wittmann']

;-------------------------------------------------------------------------------
; Solve EOS
;-------------------------------------------------------------------------------
s = EOS_VMW(taxis, paxis, $
           initial_guess = initial_guess, $
           abundances = abundances, $
           partition_function = partition_function, $
           error = error, $
           /extended, $
           specific = 1, $
           eint_zero_level = 'groundH', $
           output = 4, tp = 1)

           

;-------------------------------------------------------------------------------
; Write the results in a .sav file.
;-------------------------------------------------------------------------------
SAVE, s, file = file_tpgrid + '.sav'

;-------------------------------------------------------------------------------
; Write the results into the eos format.
;-------------------------------------------------------------------------------
record = {x : s.xyz.x, $
          z : s.xyz.z, $
          nx : N_ELEMENTS(taxis), $
          ny : N_ELEMENTS(paxis), $
          xaxis : taxis, $
          yaxis : paxis, $
          nv : 3, $
          xname : 'T', $
          yname : 'p', $
          varnames : ['rho', 'eint', 'pel'], $
          data : DBLARR(N_ELEMENTS(taxis), N_ELEMENTS(paxis), 3), $
          mask : REFORM(DBLARR(N_ELEMENTS(taxis), N_ELEMENTS(paxis))) + 1}    
; record.x = s.xyz.x
; record.z = s.xyz.z
; record.nx = nx
; record.ny = nz
; record.nv = 3
; record.xaxis = taxis
; record.yaxis = paxis
; record.xname = 'T'
; record.yname = 'p'
; record.varnames = ['rho', 'eint', 'pel']
; record.mask = REFORM(DBLARR(nx, nz), [nx, nz])
record.data[*, *, 0] = s.rho  ; density [g cm^-3]
record.data[*, *, 1] = s.etot ; specific internal energy [erg / g]
record.data[*, *, 2] = s.pel  ; electron pressure
WRITE_EOS, record, filename = file_tpgrid + '.eos'


END










