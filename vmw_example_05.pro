;==============================================================================
; VARDYA-MIHALAS-WHITTAKER EQUATION OF STATE
;------------------------------------------------------------------------------
; EXAMPLE 05: DELETE THIS. JUST A TEST
;==============================================================================

PRO vmw_example_05

ma = read_opaleos_for_mancha('/scratch/nv/Codes/Mancha3D-Cartesian/mancha_src/SIMS/Convection2D/VMW_ShelyagStyle_prhogrid_MANCHAlog_AG89_withPel_0.01pgas.mancha')
RESTORE, '/scratch/nv/EOS_PROJECT/MWEOS_v2.0/VMW_tpgrid_MANCHAlinearT_AG89_0.01pgas.sav'


; I want to compare the two for the same pressure. 
sp = s.p[0, 278] ; s.p[*, 100] = constant, s.p[100, *] = p axis
st = s.t[*, 278]


marho = ma.xaxis*1.D-3
map = ma.yaxis
mat = 10.^(REFORM(ma.data[*,278,0]))
maeint = 10.^(REFORM(ma.data[*,278,1]))*1.D4
PLOT, mat[385:550], maeint[385:550], xr = [2000, 6000], yr = [0, 2.D12], psym = -1, $
  xtit = 'T [k]', ytit = 'Eint/m [erg/gm]', tit = 'p = const = 435 [dyn/cm!U2!N]', charsi = 1.4, $
  pos = [0.10, 0.10, 0.97, 0.95]
OPLOT, s.t[*, 278], s.etot[*, 278], col = getcolor('blue')
; OPLOT, s.t[*, 278], s.etot[*, 278]-s.eh, col = getcolor('red')
; OPLOT, s.t[*, 278], s.egas[*, 278]+s.ehep[*, 278]+s.ehepp[*, 278]+s.ehp[*, 278]+s.eh2[*, 278], col = getcolor('orange')
OPLOT, s.t[*, 278], s.egas[*, 278]+s.ehp[*, 278]+s.eh2[*, 278], col = getcolor('orange')
; x(eps, p)
x = read_muram_eos(file = '/scratch/nv/idl/postmhd_v1.3/rtbl11.dat')
OPLOT, x.t[*, 20], x.eps, col = getcolor('green')

oplot, [4000, 4300], [1, 1]*1.5D12, psym = -1
xyouts, 4350, 1.48D12, 'VMW table used in MANCHA', charsi = 1.4
oplot, [4000, 4300], [1, 1]*1.4D12, col = getcolor('blue')
xyouts, 4350, 1.38D12, 'VMW computed, s.etot', charsi = 1.4
oplot, [4000, 4300], [1, 1]*1.3D12, col = getcolor('orange')
xyouts, 4350, 1.28D12, 'VMW computed, s.egas + s.ehp + s.eh2', charsi = 1.4
oplot, [4000, 4300], [1, 1]*1.2D12, col = getcolor('green')
xyouts, 4350, 1.18D12, 'Table used in MURaM', charsi = 1.4
WRITE_PNG, 'MANCHA_vs_MURaM_vs_VMWcode.png', tvrd(/true)

stop
END










