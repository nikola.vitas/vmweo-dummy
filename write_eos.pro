;+
; NAME:
;       WRITE_EOS
;
; PURPOSE:
;
;       This function writes the results of an EOS to a file. The file
;       contains data for given X and Z, two grid axes (T and p, for
;       example) and a sequence of data tables (up to 11 of them). The
;       file also contain meta information on the axes and data blocks
;       that are saved. The main purpose is to have the EOS results in
;       a unique handy format for the comparison between different
;       equations of state.
;      
; AUTHOR:
;
;       Nikola Vitas
;       Instituto de Astrofisica de Canarias (IAC)
;       C/ Via Lactea, s/n
;       E38205 - La Laguna (Tenerife), Espana
;       Email: n.vitas@iac.es, nikola.vitas@gmail.com
;       Homepage: nikolavitas.blogspot.com
;
; CATEGORY:
;
;       Numerics.
;
; CALLING SEQUENCE:
;
;       WRITE_EOS, data, filename = filename
;
; INPUTS:
;
;       data     = Structure. See READ_OPALEOS for the definition.
; 
;       filename = String, Name of the input file.
; 
; OUTPUTS:
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; EXAMPLE:
;
; DEPENDENCIES:
;
; MODIFICATION HISTORY:
;
;       Written by Nikola Vitas, June 2013.
;                  - Changed from WRITE_OPALEOS into more general WRITE_EOS.
;-
;================================================================================
; WRITE_EOS, IDL routine by Nikola Vitas is licensed under a Creative Commons 
; Attribution-NonCommercial-ShareAlike 3.0 Unported License.
;
; This software is provided by NV ''as is'' and any express or implied warranties, 
; including, but not limited to, the implied warranties of merchantability and 
; fitness for a particular purpose are disclaimed. In no event shall NV be liable 
; for any direct, indirect, incidental, special, exemplary, or consequential 
; damages (including, but not limited to, procurement of substitute goods or 
; services; loss of use, data, or profits; loss of use, data, or profits; or 
; business interruption) however caused and on any theory of liability, whether 
; in contract, strict liability, or tort (including negligence or otherwise) 
; arising in any way out of the use of this software, even if advised of the 
; possibility of such damage.
;================================================================================

PRO write_eos, record, filename = filename

COMPILE_OPT idl2, HIDDEN

PRINT, ' > Writing ' + filename

x = DOUBLE(record.x)
z = DOUBLE(record.z)
nvar = FIX(record.nv)
nx = FIX(record.nx)
ny = FIX(record.ny)
xname = record.xname
yname = record.yname
varnames = record.varnames
xaxis = DOUBLE(record.xaxis)
yaxis = DOUBLE(record.yaxis)
data = DOUBLE(record.data)
mask = DOUBLE(record.mask)

a = 'Y'
IF (FILE_TEST(filename)) THEN BEGIN
  PRINT, ' # File ' + filename + ' exists. Overwrite?'
  a = ''
  READ, a, prompt = '(Y/N) '
ENDIF 

IF a EQ 'Y' THEN BEGIN
    xnamestr = ''
    READS, xname, xnamestr, format = '(A16)'
    ynamestr = ''
    READS, yname, ynamestr, format = '(A16)'

    OPENW, un, filename, /get_lun, /binary
    WRITEU, un, x
    WRITEU, un, z
    WRITEU, un, nx
    WRITEU, un, ny
    WRITEU, un, nvar
    WRITEU, un, xnamestr
    WRITEU, un, ynamestr
    FOR iv = 0, nvar-1 DO BEGIN
      namestr = ''
      READS, varnames[iv], namestr, format = '(A16)'
      WRITEU, un, namestr
    ENDFOR
    WRITEU, un, xaxis
    WRITEU, un, yaxis
    WRITEU, un, mask
    FOR iv = 0, nvar-1 DO $
      WRITEU, un, REFORM(data[*, *, iv])
    FREE_LUN, un
ENDIF   

END
