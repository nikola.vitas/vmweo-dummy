FUNCTION dsign, a, b
IF B GE 0 THEN d = ABS(a) ELSE d = -ABS(a)
RETURN, d
END
