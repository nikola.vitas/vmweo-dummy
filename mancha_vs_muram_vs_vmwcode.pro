; This routine demonstrates that the EOS tables used for MANCHA
; are properly done regarding the internal energy and that they
; match the tables of MURaM. Moreover, I show how these files
; correspond to the results of the VMW code (and how the energy
; is constructed for MANCHA VMW tables. It's recommendable to 
; try other Eint component saved in .sav files and to inspect
; their contribution to the total Eint.
; 
; This routine is written on April, 12, 2016 while searching for
; the cause of the glitch in the MANCHA convection.

; The reference pressure in comparison from April, 12, 2006 was 
; 435 dyn/cm2. 
PRO mancha_vs_muram_vs_vmwcode

ma = READ_H5EOS('/home/nikola/Data/EOS/MANCHA/h5/VMW_prhogrid_14062016_AG89.h5')

; This table is obsolete and irrelevant for further tests:
; read_opaleos_for_mancha('/scratch/nv/Codes/Mancha3D-Cartesian/mancha_src/SIMS/Convection2D/VMW_ShelyagStyle_prhogrid_MANCHAlog_AG89_withPel_0.01pgas.mancha')

; This is the old, smaller range table
; ; results of the VMW computation. Old one - structure s
; RESTORE, '/home/nikola/EOS_PROJECT/MWEOS_v2.0/VMW_tpgrid_MANCHAlinearT_AG89_0.01pgas.sav'
; ; I want to compare the two for the same pressure. 
; sp = s.p[0, 278] ; s.p[*, 100] = constant, s.p[100, *] = p axis
; st = s.t[*, 278]


 
RESTORE, '/home/nikola/IDL/VMW_FOR_MANCHA/Compute_VMW/VMW_tpgrid_MANCHATrhoextended_0.01pgas.sav'

; x(eps, p)
x = read_muram_eos(file = '/home/nikola/IDL/postmhd_v1.3/rtbl11.dat')

; p0 = 453.
p0 = 140.

idp = VALUE_LOCATE(REFORM(s.p[0, *]), p0)
idp_muram = VALUE_LOCATE(x.p, p0)


; I want to compare the two for the same pressure. 
sp = s.p[0, idp] ; s.p[*, 100] = constant, s.p[100, *] = p axis
st = s.t[*, idp]




marho = ma.xaxis*1.D-3
map = ma.yaxis
mat = 10.^(REFORM(ma.data[*,idp,0]))
idt0 = MIN(WHERE(mat NE 0))
idt1 = MAX(WHERE(mat NE 0))

maeint = 10.^(REFORM(ma.data[*,idp,1]))*1.D4
PLOT, mat[idt0:idt1], maeint[idt0:idt1], xr = [2000, 6000], yr = [0, 2.D12], psym = -1, $
  xtit = 'T [k]', ytit = 'Eint/m [erg/gm]', tit = 'p = const = '+STRING(p0, FORMAT='(F6.0)')+' [dyn/cm!U2!N]', charsi = 1.4, $
  pos = [0.10, 0.10, 0.97, 0.95]
OPLOT, s.t[*, idp], s.etot[*, idp], col = getcolor('blue')
; OPLOT, s.t[*, 278], s.etot[*, 278]-s.eh, col = getcolor('red')
; OPLOT, s.t[*, 278], s.egas[*, 278]+s.ehep[*, 278]+s.ehepp[*, 278]+s.ehp[*, 278]+s.eh2[*, 278], col = getcolor('orange')
OPLOT, s.t[*, idp], s.egas[*, idp]+s.ehp[*, idp]+s.eh2[*, idp], col = getcolor('orange')

OPLOT, x.t[*, idp_muram], x.eps, col = getcolor('green')

oplot, [4000, 4300], [1, 1]*1.5D12, psym = -1
xyouts, 4350, 1.48D12, 'VMW table used in MANCHA', charsi = 1.4
oplot, [4000, 4300], [1, 1]*1.4D12, col = getcolor('blue')
xyouts, 4350, 1.38D12, 'VMW computed, s.etot', charsi = 1.4
oplot, [4000, 4300], [1, 1]*1.3D12, col = getcolor('orange')
xyouts, 4350, 1.28D12, 'VMW computed, s.egas + s.ehp + s.eh2', charsi = 1.4
oplot, [4000, 4300], [1, 1]*1.2D12, col = getcolor('green')
xyouts, 4350, 1.18D12, 'Table used in MURaM', charsi = 1.4
stop
END
