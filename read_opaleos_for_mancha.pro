;+
; NAME:
;       READ_OPALEOS_FOR_MANCHA
;
; PURPOSE:
;
;       This function reads the OPAL EOS files produced by the 
;       IDL OPAL library. The files contain data for fixed X and Z, two
;       grid axes (T and p, for example) and a sequence of data tables
;       (up to 11 of them). The files also contain information on what
;       axes and data blocks are saved.
;      
; AUTHOR:
;
;       Nikola Vitas
;       Instituto de Astrofisica de Canarias (IAC)
;       C/ Via Lactea, s/n
;       E38205 - La Laguna (Tenerife), Espana
;       Email: n.vitas@iac.es, nikola.vitas@gmail.com
;       Homepage: nikolavitas.blogspot.com
;
; CATEGORY:
;
;       Numerics.
;
; CALLING SEQUENCE:
;
;       eos = READ_OPALEOS_FOR_MANCHA(filename)
;
; INPUTS:
;
;       filename = String, Name of the input file.
; 
; OUTPUTS:
;
;       eos      = Structure.
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; EXAMPLE:
;
; DEPENDENCIES:
;
; MODIFICATION HISTORY:
;
;       Written by Nikola Vitas, June 2013.
;-
;================================================================================
; READ_OPALEOS_FOR_MANCHA, IDL routine by Nikola Vitas is licensed under a 
; Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
;
; This software is provided by NV ''as is'' and any express or implied warranties, 
; including, but not limited to, the implied warranties of merchantability and 
; fitness for a particular purpose are disclaimed. In no event shall NV be liable 
; for any direct, indirect, incidental, special, exemplary, or consequential 
; damages (including, but not limited to, procurement of substitute goods or 
; services; loss of use, data, or profits; loss of use, data, or profits; or 
; business interruption) however caused and on any theory of liability, whether 
; in contract, strict liability, or tort (including negligence or otherwise) 
; arising in any way out of the use of this software, even if advised of the 
; possibility of such damage.
;================================================================================

FUNCTION read_opaleos_for_mancha, filename

; COMPILE_OPT idl2, HIDDEN

filesize = (FILE_INFO(filename)).size

dummy_d = 0.D
dummy_i = 0L
dummy_s = STRJOIN(STRING(FLTARR(16), format = '(i1)'))

nv = 2

OPENR, un, filename, /get_lun, /binary
READU, un, dummy_d
x = dummy_d
READU, un, dummy_d
z = dummy_d
READU, un, dummy_i
nx = dummy_i
READU, un, dummy_i
ny = dummy_i

xaxis = DBLARR(nx)
yaxis = DBLARR(ny)

READU, un, xaxis
READU, un, yaxis

; guess the number of variables
nv = ROUND(filesize - (nx+ny)*8 + 2*4. + 2*8)/(nx*ny*8)

data = DBLARR(nx, ny, nv)
dummy = DBLARR(nx, ny)

FOR iv = 0, nv-1 DO BEGIN
  READU, un, dummy
  data[*, *, iv] = dummy
ENDFOR
  
data = REFORM(data, nx, ny, nv)
  
FREE_LUN, un

result = {x:0.D, z:0.D, nx:0, ny:0, nv:0, xaxis:FLTARR(nx), yaxis:FLTARR(ny), $
         data:DBLARR(nx, ny, nv)}
result.x = x
result.z = z
result.nx = nx
result.ny = ny
result.nv = nv
result.xaxis = xaxis
result.yaxis = yaxis
result.data = REFORM(data, nx, ny, nv)

RETURN, result

END
