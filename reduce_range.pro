;+
; NAME:
;       REDUCE_RANGE
;
; PURPOSE:
;
;       This function computes biquadratic interpolation for given argument
;       between four given points. The function uses LAGRANGE routine
;       for the linear interpolation between the three pairs of points.
;      
; AUTHOR:
;
;       Nikola Vitas
;       Instituto de Astrofisica de Canarias (IAC)
;       C/ Via Lactea, s/n
;       E38205 - La Laguna (Tenerife), Espana
;       Email: n.vitas@iac.es, nikola.vitas@gmail.com
;       Homepage: nikolavitas.blogspot.com
;
; CATEGORY:
;
;       Numerics.
;
; CALLING SEQUENCE:
;
;       y = BILINEAR(x, y, xx, yy, zz)
;
; INPUTS:
;
;       x     = Scalar, float. Argument along x-axis.
; 
;       y     = Scalar, float. Argument along y-axis.
;
;       xx    = Array[3], float. X coordinates of the defining points.
;
;       yy    = Array[3], float. Y coordinates of the defining points.
; 
;       zz    = Array[3, 3], float. Values of the function to be interpolated.
; 
; OUTPUTS:
;
;       z     = Scalar, float. Result of the interpolation
;
; INPUT KEYWORDS:
;
; OUTPUT KEYWORDS:
;
; EXAMPLE:
;
;       zz = [[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]]
;       xx = [0., 1., 2.]
;       yy = [0., 1., 2.]
;       x = 1.5
;       y = 1.5
;       z = BIQUADRATIC(x, y, xx, yy, zz)
;
; DEPENDENCIES:
;
;       LAGRANGE
;
; MODIFICATION HISTORY:
;
;       Written by Nikola Vitas, October 2007.
;       - small changes and fixes, NV, June 2013.
;       - TO-DO: Check the size of the input arrays
;-
;================================================================================
; BIQUADRATIC, IDL routine by Nikola Vitas is licensed under a Creative Commons 
; Attribution-NonCommercial-ShareAlike 3.0 Unported License.
;
; This software is provided by NV ''as is'' and any express or implied warranties, 
; including, but not limited to, the implied warranties of merchantability and 
; fitness for a particular purpose are disclaimed. In no event shall NV be liable 
; for any direct, indirect, incidental, special, exemplary, or consequential 
; damages (including, but not limited to, procurement of substitute goods or 
; services; loss of use, data, or profits; loss of use, data, or profits; or 
; business interruption) however caused and on any theory of liability, whether 
; in contract, strict liability, or tort (including negligence or otherwise) 
; arising in any way out of the use of this software, even if advised of the 
; possibility of such damage.
;================================================================================

FUNCTION reduce_range, x, x0, x1

;result = (x > x0) < x1

IF (x LT x0) THEN x = x0
IF (x GT x1) THEN x = x1


RETURN, x
END 
